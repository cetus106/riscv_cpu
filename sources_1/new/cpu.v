`include "define.v"

module CPU(
    input  wire clk,
    input  wire rst,
    //data from and to ROM
    input  wire [`InstBus]     rom_data_i,
    output wire                rom_ce_o,
    output wire [`InstAddrBus] rom_addr_o,
    //data from and to RAM
    input  wire [`DataBus]     ram_data_i,
    output wire                ram_ce_o,
    output wire [`ByteSelBus]  ram_we_o,
    output wire [`DataAddrBus] ram_addr_o,
    output wire [`DataBus]     ram_data_o
    );
    
    //connect pc with id
    wire[`InstAddrBus]  pc;
    wire[`InstAddrBus]  id_pc_i;
    wire[`InstBus]      id_inst_i;
    wire                branch_flag;
    wire[`RegBus]       branch_target_address;
    
    //connect stop with id/ex
    wire                stop_flag;
    
    //connect ex with id
    wire                stallreq;
    wire[`AluSelBus]    ex_id_alusel;
    
    //connect id with id/ex
    wire[`AluOpBus]     id_aluop_o;
    wire[`AluSelBus]    id_alusel_o;
    wire[`RegBus]       id_reg1_o;
    wire[`RegBus]       id_reg2_o;
    wire                id_wreg_o;
    wire[`RegAddrBus]   id_wd_o;
    wire[`RegBus]       id_link_addr_o;
    wire[`InstBus]      id_inst_o;
    
    //connect id/ex with ex
    wire[`AluOpBus]     ex_aluop_i;
    wire[`AluSelBus]    ex_alusel_i;
    wire[`RegBus]       ex_reg1_i;
    wire[`RegBus]       ex_reg2_i;
    wire                ex_wreg_i;
    wire[`RegAddrBus]   ex_wd_i;
    wire[`RegBus]       ex_link_addr_i;
    wire[`InstBus]      ex_inst_i;     
    
    //connect ex with ex/mem
    wire                ex_wreg_o;
    wire[`RegAddrBus]   ex_wd_o;
    wire[`RegBus]       ex_wdata_o;
    wire[`AluOpBus]     ex_aluop_o;
    wire[`RegBus]       ex_mem_addr_o;
    wire[`RegBus]       ex_reg2_o;
    
    //connect ex/mem with mem
    wire                mem_wreg_i;
    wire[`RegAddrBus]   mem_wd_i;
    wire[`RegBus]       mem_wdata_i;
    wire[`AluOpBus]     mem_aluop_i;
    wire[`RegBus]       mem_mem_addr_i;
    wire[`RegBus]       mem_reg2_i;

    //connect mem with RAM
    wire[`RegBus]       mem_mem_data_i;
    
    //connect mem with mem/wb
    wire                mem_wreg_o;
    wire[`RegAddrBus]   mem_wd_o;
    wire[`RegBus]       mem_wdata_o;
    wire[`RegBus]       mem_mem_addr_o;
    wire                mem_mem_we_o;
    wire[`ByteSelBus]   mem_mem_sel_o;
    wire[`RegBus]       mem_mem_data_o;
    wire                mem_mem_ce_o;
    
    //connect mem/wb with wb
    wire                wb_wreg_i;
    wire[`RegAddrBus]   wb_wd_i;
    wire[`RegBus]       wb_wdata_i; 
    
    //connect id with regfile
    wire                reg1_read;
    wire                reg2_read;
    wire[`RegBus]       reg1_data;
    wire[`RegBus]       reg2_data;
    wire[`RegAddrBus]   reg1_addr;
    wire[`RegAddrBus]   reg2_addr;
    
    //realize PC
    pc_reg pc_reg0(
        .clk(clk),    .rst(rst),       .pc(pc),    .ce(rom_ce_o),
        .branch_flag_i(branch_flag),   .branch_target_address_i(branch_target_address),
        .stallreq(stallreq)
    );
    assign rom_addr_o = pc;
    
    //realize IF/ID
    if_id if_id0(
        .clk(clk),   .rst(rst),    .if_pc(pc),
        .if_inst(rom_data_i), .id_pc(id_pc_i),
        .id_inst(id_inst_i),
        .stallreq(stallreq)
    );
    
    //realize ID
    id id0(
        .rst(rst),   .pc_i(id_pc_i),     .inst_i(id_inst_i),
        //data from regfile
        .reg1_data_i(reg1_data),    .reg2_data_i(reg2_data),
        //data to regfile
        .reg1_read_o(reg1_read),    .reg2_read_o(reg2_read),
        .reg1_addr_o(reg1_addr),    .reg2_addr_o(reg2_addr),
        //data to ex module
        .aluop_o(id_aluop_o),       .alusel_o(id_alusel_o),
        .reg1_o(id_reg1_o),         .reg2_o(id_reg2_o),
        .wd_o(id_wd_o),             .wreg_o(id_wreg_o),
        .link_addr_o(id_link_addr_o),
        .inst_o(id_inst_o),
        //data from ex module
        .ex_wdata_i(ex_wdata_o),    .ex_wd_i(ex_wd_o),      .ex_wreg_i(ex_wreg_o),  .ex_alusel_i(ex_id_alusel),
        //data from mem
        .mem_wdata_i(mem_wdata_o),  .mem_wd_i(mem_wd_o),    .mem_wreg_i(mem_wreg_o),
        
        .branch_flag_o(branch_flag),.branch_target_address_o(branch_target_address),
        //signal to stop
        .stallreq(stallreq), .stop_flag_i(stop_flag)
    );
    
    //realize stop
    stop stop0(.clk(clk),   .rst(rst),  .branch_flag_i(branch_flag),    .stop_flag_o(stop_flag) ,.stallreq(stallreq));
    
    //realize Regfile
    regfile regfile0(
        .clk(clk),  .rst(rst),
        //write port
        .we(wb_wreg_i),     .waddr(wb_wd_i),    .wdata(wb_wdata_i),
        //read port1
        .re1(reg1_read),    .raddr1(reg1_addr),     .rdata1(reg1_data),
        //read port2
        .re2(reg2_read),    .raddr2(reg2_addr),     .rdata2(reg2_data)
    );
    
    //realize ID/EX
    id_ex id_ex0(
        .clk(clk),   .rst(rst),
        //data from id
        .id_aluop(id_aluop_o),  .id_alusel(id_alusel_o),
        .id_reg1(id_reg1_o),    .id_reg2(id_reg2_o),
        .id_wd(id_wd_o),        .id_wreg(id_wreg_o),
        .id_link_address(id_link_addr_o), .if_stop_i(stop_flag),
        .id_inst(id_inst_o),    .stallreq(stallreq),
        //data to ex
        .ex_aluop(ex_aluop_i),  .ex_alusel(ex_alusel_i),
        .ex_reg1(ex_reg1_i),    .ex_reg2(ex_reg2_i),
        .ex_wd(ex_wd_i),        .ex_wreg(ex_wreg_i),
        .ex_link_address(ex_link_addr_i),
        .ex_inst(ex_inst_i)
    );
    
    //realize EX
    ex ex0(
        .rst(rst),
        //data from id module
        .aluop_i(ex_aluop_i),   .alusel_i(ex_alusel_i),
        .reg1_i(ex_reg1_i),     .reg2_i(ex_reg2_i),
        .wd_i(ex_wd_i),         .wreg_i(ex_wreg_i),
        .link_address_i(ex_link_addr_i),
        .inst_i(ex_inst_i),
        //data to id
        .alusel_o(ex_id_alusel),
        //Result of execution
        .wd_o(ex_wd_o),         .wreg_o(ex_wreg_o),         .wdata_o(ex_wdata_o),
        .aluop_o(ex_aluop_o),   .mem_addr_o(ex_mem_addr_o), .reg2_o(ex_reg2_o)
    );
    
    //realize EX/MEM
    ex_mem ex_mem0(
        .clk(clk),  .rst(rst),
        //data from ex module
        .ex_wd(ex_wd_o),        .ex_wreg(ex_wreg_o),        .ex_wdata(ex_wdata_o),
        .ex_aluop(ex_aluop_o),  .ex_mem_addr(ex_mem_addr_o),.ex_reg2(ex_reg2_o), 
        //data to mem module
        .mem_wd(mem_wd_i),       .mem_wreg(mem_wreg_i),         .mem_wdata(mem_wdata_i),
        .mem_aluop(mem_aluop_i), .mem_mem_addr(mem_mem_addr_i), .mem_reg2(mem_reg2_i)
    );
    
    //realize MEM
    mem mem0(
        .rst(rst),
        //data from ex module
        .wd_i(mem_wd_i),        .wreg_i(mem_wreg_i),    .wdata_i(mem_wdata_i),
        .aluop_i(mem_aluop_i),  .reg2_i(mem_reg2_i),    .mem_addr_i(mem_mem_addr_i),
        .mem_data_i(mem_mem_data_i),
        //result
        .wd_o(mem_wd_o),                .wreg_o(mem_wreg_o),        .wdata_o(mem_wdata_o),
        .mem_addr_o(mem_mem_addr_o),    .mem_sel_o(mem_mem_sel_o),  
        .mem_data_o(mem_mem_data_o),    .mem_ce_o(mem_mem_ce_o)
    );
    //----------realize conection with RAM----------
    assign mem_mem_data_i    =   ram_data_i;
    assign ram_addr_o        =   mem_mem_addr_o;
    assign ram_we_o          =   mem_mem_sel_o;
    assign ram_ce_o          =   mem_mem_ce_o;
    assign ram_data_o        =   mem_mem_data_o;

    //realize MEM/WB
    mem_wb mem_wb0(
        .clk(clk),  .rst(rst),
        //data from mem module
        .mem_wd(mem_wd_o),  .mem_wreg(mem_wreg_o),  .mem_wdata(mem_wdata_o),
        //data to wb module
        .wb_wd(wb_wd_i),    .wb_wreg(wb_wreg_i),    .wb_wdata(wb_wdata_i)
    );
endmodule
