`include "define.v"

module pc_reg(
    input wire                  clk,
    input wire                  rst,
    
    //jump instuction from id
    input wire                  branch_flag_i,
    input wire [`RegBus]        branch_target_address_i,
    
    //loadrelate message from id
    input wire                  stallreq,
    
    output reg [`InstAddrBus]   pc,
    output reg                  ce
    );
    
    always@(posedge clk) begin
        if(rst == `RstEnable) begin
            ce <= `ChipDisable;   //ban the Rom if rst
        end else begin
            ce <= `ChipEnable;    //enable the Rom when rst is over
        end
    end
    always@(posedge clk) begin
        if(ce == `ChipDisable) begin
            pc <= `ZeroWord;      //pc=0 if rst
        end else if(stallreq == `Stop) begin
            pc <= pc;
        end else if(branch_flag_i == `Branch) begin
            pc <= branch_target_address_i;
        end else begin
            pc <= pc + 32'h4;     //pc+=4 every cycle when Rom is enabled
        end 
    end
endmodule

