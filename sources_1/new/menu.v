module MENU(
    input         clk_u,    //clock for state update
    input         clk_d,    //clock for vga display
    input         clk_f,    //clock for flash
    input         rst,
    input         ena, 
    input  [1:0]  ctrl,
    output [13:0] vga
    );

    wire hsync;
    wire vsync;
    wire [11:0] RGB;
    assign vga[13] = hsync;
    assign vga[12] = vsync;
    assign vga[11:0] = RGB;
    
    reg  [1:0] mode;

    wire [9:0] hcount;
    wire [9:0] vcount;
    wire dat_act;       // to show if the pixel is in the screen
    
    wire word_RGB;
    wire box_RGB;
    wire top_RGB;

    always @(posedge clk_u)
    begin
        if(rst)
            mode <= 2'b00;
        else if(ena)
            mode <= ctrl;
        else
            mode <= mode;
    end

    sync_create sync(.clk(clk_d),.hcount(hcount),.vcount(vcount),.hsync(hsync),.vsync(vsync),.dat_act(dat_act));
    word word0(.clk(clk_d),.hcount(hcount),.vcount(vcount),.word_data(word_RGB));
    box box0(.if_shimmering(clk_f),.ctrl(mode),.hcount(hcount),.vcount(vcount),.box_data(box_RGB));
    
    assign top_RGB = box_RGB | word_RGB;
    
    assign RGB=(dat_act)?(top_RGB?12'h000:12'h0ff):12'h000;
endmodule
