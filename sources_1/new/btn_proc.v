module BTN_PROC(
    input  clk,
    input  rst,
    input  ena,
    input  din,
    output dout
    );          //three segment finite state machine

    reg [6:0]  pst_stt;
    reg [6:0]  nxt_stt;

    always @(posedge clk)       //state update
    begin
        if(rst)
            pst_stt <= 7'd0;
        else
            pst_stt <= nxt_stt;
    end

    always @(*)                 //excitation
    begin
        if(pst_stt == 7'd0)
            if(din)
                nxt_stt = 7'd1;
            else
                nxt_stt = 7'd0;
        else if(pst_stt == 7'd126)
            if(din)
                nxt_stt = 7'd127;
            else
                nxt_stt = 7'd0;
        else if(pst_stt == 7'd127)
            if(din)
                nxt_stt = 7'd127;
            else
                nxt_stt = 7'd0;
        else
            if(!ena)
                nxt_stt = pst_stt + 7'd1;
            else if(din)
                nxt_stt = 7'd127;
            else
                nxt_stt = 7'd0;
    end

    assign dout = ~( &pst_stt || &(~pst_stt) ); //output
endmodule
