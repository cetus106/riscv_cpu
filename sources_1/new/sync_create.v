module sync_create(
    input clk,
    output reg [9:0] hcount,
    output reg [9:0] vcount,
    output hsync,
    output vsync,
    output dat_act
);
    parameter hsync_end = 10'd95,      

              hdat_begin = 10'd143,    
              hdat_end = 10'd783,       
              hpixel_end = 10'd799,     

              vsync_end = 10'd1,

              vdat_begin = 10'd34,
              vdat_end = 10'd514,
              vline_end = 10'd524;
    
    wire hcount_ov;         
    wire vcount_ov;          

    always @(posedge clk)
    begin
        if (hcount_ov)
            hcount <= 10'd0;
        else
            hcount <= hcount + 10'd1;
    end
    assign hcount_ov = (hcount == hpixel_end);

    always @(posedge clk)
    begin
        if (hcount_ov)
            begin
            if (vcount_ov)
                vcount <= 10'd0;
            else
                vcount <= vcount + 10'd1;
        end
    end
    assign vcount_ov = (vcount == vline_end);

    assign dat_act = ((hcount >= hdat_begin) && (hcount < hdat_end)) &&
                 ((vcount >= vdat_begin) && (vcount < vdat_end));
    assign hsync = (hcount > hsync_end);
    assign vsync = (vcount > vsync_end);
endmodule