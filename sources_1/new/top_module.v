`include "define.v"

module top_module(
    input         clk,    //100MHz clock signal from crystal oscillator
    input         rst,    //Asynchronous reset signal, from sw[15]
    input  [ 4:0] btn,    //Central--4, up--3, down--2, left--1, right--0
    input  [14:0] sw,
    output [15:0] led,
    output [11:0] tube,   //[11:8]--digits, [7:0]--segments & dots
    output [13:0] vga     //13--hsync, 12--vsync, [11:0]--rgb
    );

    //generated clock
    wire                clk_cpu;    //37MHz clock for main modules
    wire                clk_mem;    //74MHz clock for ROM and RAM
    wire                clk_vga;    //25MHz clock for VGA display
    wire                clk_tube;   //200Hz clock for digital tube
    wire                clk_flash;  //1Hz   clock for flash

    //connect CPU with BUS
    wire                cpu_rom_ce;
    wire [`InstAddrBus] cpu_rom_addr;
    wire [`InstBus]     cpu_rom_data;
    wire                cpu_ram_ce;
    wire [`ByteSelBus]  cpu_ram_we;
    wire [`DataAddrBus] cpu_ram_addr;
    wire [`DataBus]     cpu_ram_din;
    wire [`DataBus]     cpu_ram_dout;

    //connect ROM with BUS
    wire                rom_ce;
    wire [`RomAddrBus]  rom_addr;
    wire [`InstBus]     rom_data;

    //connect RAM with BUS
    wire                ram_ce;
    wire [`ByteSelBus]  ram_we;
    wire [`RamAddrBus]  ram_addr;
    wire [`DataBus]     ram_din;
    wire [`DataBus]     ram_dout;

    //connect BTN with BUS
    wire [4:0]          btn_ena;
    wire [4:0]          btn_data;

    //connect LED with BUS
    wire                led_ena;
    wire [`LedBus]      led_din;

    //connect TUBE with BUS
    wire                tube_ena;              
    wire [`TubeDinBus]  tube_din;

    //connect MENU with BUS
    wire                menu_ena;
    wire [1:0]          menu_ctrl;

    //instantiation of clock generators
    CLK_GEN CLK_GEN0(
        .clk_i(clk),        .clk_o1(clk_cpu),   .clk_o2(clk_mem)
    );
    CLK_GEN_1 CLK_GEN1(
        .clk_i(clk),        .clk_o(clk_vga)
    );
    CLK_GEN_LOW #(50_0000) CLK_GEN_LOW0 (
        .clk_i(clk),        .rst(rst),          .clk_o(clk_tube)
    );
    CLK_GEN_LOW #(200)     CLK_GEN_LOW1 (
        .clk_i(clk_tube),   .rst(rst),          .clk_o(clk_flash)
    );

    //instantiation of CPU
    CPU CPU0(
        .clk(clk_cpu),  .rst(rst),
        //data from and to ROM
        .rom_data_i(cpu_rom_data),  .rom_ce_o(cpu_rom_ce),  .rom_addr_o(cpu_rom_addr),
        //data from and to RAM
        .ram_data_i(cpu_ram_dout),  .ram_ce_o(cpu_ram_ce),  .ram_we_o(cpu_ram_we),
        .ram_addr_o(cpu_ram_addr),  .ram_data_o(cpu_ram_din)
    );

    //instantiation of ROM
    ROM ROM0(
        .clka(clk_mem),     .ena(rom_ce),
        .addra(rom_addr),   .douta(rom_data)
    );

    //instantiation of RAM
    RAM RAM0(
        .clka(clk_mem),     .ena(ram_ce),       .wea(ram_we),
        .addra(ram_addr),   .dina(ram_din),     .douta(ram_dout)
    );

    //instantiation of BUS
    BUS BUS0(
        //data from and to CPU
        .cpu_rom_ce_i(cpu_rom_ce),      .cpu_rom_addr_i(cpu_rom_addr),
        .cpu_rom_data_o(cpu_rom_data),  
        .cpu_ram_ce_i(cpu_ram_ce),      .cpu_ram_we_i(cpu_ram_we),
        .cpu_ram_addr_i(cpu_ram_addr),  .cpu_ram_din_i(cpu_ram_din),
        .cpu_ram_dout_o(cpu_ram_dout),
        //data from and to ROM
        .rom_ce_o(rom_ce),  .rom_addr_o(rom_addr),  .rom_data_i(rom_data),
        //data from and to RAM
        .ram_ce_o(ram_ce),  .ram_we_o(ram_we),      .ram_addr_o(ram_addr),
        .ram_din_o(ram_din),.ram_dout_i(ram_dout),
        //data from input
        .sw_i(sw),          .btn_ena_o(btn_ena),    .btn_data_i(btn_data),
        //data to output
        .led_ena_o(led_ena),    .led_din_o(led_din),
        .tube_ena_o(tube_ena),  .tube_din_o(tube_din),
        .menu_ena_o(menu_ena),  .menu_ctrl_o(menu_ctrl)
    );

    //instantiation of BTN
    BTN BTN0(
        .clk(clk_cpu),  .rst(rst),  .ena(btn_ena),
        .din(btn),      .dout(btn_data)
    );

    //instantiation of LED
    LED LED0(
        .clk(clk_cpu),  .rst(rst),  .ena(led_ena),
        .din(led_din),  .led(led)
    );

    //instantiation of TUBE
    TUBE TUBE0(
        .clk_u(clk_cpu),  .clk_d(clk_tube),  .clk_f(clk_flash),   
        .rst(rst),        .ena(tube_ena),    .din(tube_din),    .dout(tube)
    );

    //instantiation of MENU
    MENU MENU0(
        .clk_u(clk_cpu),  .clk_d(clk_vga),   .clk_f(clk_flash),   
        .rst(rst),        .ena(menu_ena),    .ctrl(menu_ctrl),    .vga(vga)
    );

endmodule
