`include "define.v"

module ex_mem(
    input wire clk,
    input wire rst,
    
    //data from ex module
    input wire [`RegAddrBus]    ex_wd,
    input wire                  ex_wreg,
    input wire [`RegBus]        ex_wdata,
    
    //data to mem module
    output reg [`RegAddrBus]    mem_wd,
    output reg                  mem_wreg,
    output reg [`RegBus]        mem_wdata,

    //in out port for store and load operation
    input wire[`AluOpBus]       ex_aluop,
    input wire[`RegBus]         ex_mem_addr,
    input wire[`RegBus]         ex_reg2,

    output reg[`AluOpBus]       mem_aluop,
    output reg[`RegBus]         mem_reg2,
    output reg[`RegBus]         mem_mem_addr
    );
    always@(posedge clk) begin
        if(rst == `RstEnable) begin
            mem_wd      <= `NOPRegAddr;
            mem_wreg    <= `WriteDisable;
            mem_wdata   <= `ZeroWord;
            mem_aluop   <= `EXE_NOP_OP;
            mem_reg2    <= `ZeroWord;
            mem_mem_addr<= `ZeroWord;
        end else begin
            mem_wd      <= ex_wd;
            mem_wreg    <= ex_wreg;
            mem_wdata   <= ex_wdata;
            mem_aluop   <= ex_aluop;
            mem_reg2    <= ex_reg2;
            mem_mem_addr<= ex_mem_addr;
        end

    end
endmodule
