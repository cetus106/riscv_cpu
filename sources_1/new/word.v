module word(
    input clk,
    input wire [9:0] hcount,
    input wire [9:0] vcount,
    output word_data
    );
    reg [18:0] dir;
    always @(posedge clk ) begin
        dir <=10'd640*(vcount-34)+hcount-143;
    end
    word_picture word_picture0(.clka(clk),.addra(dir),.douta(word_data));
endmodule
