`include "define.v"

module LED(
    input                clk,
    input                rst,
    input                ena,    //Signal of changing data
    input      [`LedBus] din,
    output reg [`LedBus] led
    );
    always @(posedge clk)
    begin
        if(rst == `RstEnable)
            led <= `ZeroHalf;
        else if(ena == `WriteEnable)
            led <= din;
        else
            led <= led;
    end
endmodule
