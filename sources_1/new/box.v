
module box(
    input wire if_shimmering,
    input wire [9:0] hcount,
    input wire [9:0] vcount,
    input [1:0] ctrl,
    output box_data
    );
    wire [9:0] hdir;
    wire [9:0] vdir;
    wire light_box;
    wire number_box;
    wire box;

    assign hdir = hcount - 143;
    assign vdir = vcount - 34;
    assign light_box = ((hdir>=203 && hdir < 437)&&(vdir == 229 ||vdir ==322)) || ((vdir>=229 && vdir < 322)&&(hdir == 203 ||hdir ==437));
    assign number_box = ((hdir>=203 && hdir < 437)&&(vdir == 340 ||vdir ==433)) || ((vdir>=340 && vdir < 433)&&(hdir == 203 ||hdir ==437));
    assign box =ctrl[1]?number_box:light_box;
    assign box_data = ctrl[0]?box:(box&if_shimmering);
    

endmodule
