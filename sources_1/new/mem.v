`include "define.v"
module mem(
    input wire rst,
    
    //data from ex module
    input wire [`RegAddrBus]    wd_i,
    input wire                  wreg_i,
    input wire [`RegBus]        wdata_i,
    
    //result
    output reg [`RegAddrBus]    wd_o,
    output reg                  wreg_o,
    output reg [`RegBus]        wdata_o,

    //port for store and load operation
    input wire[`AluOpBus]       aluop_i,
    input wire[`RegBus]         mem_addr_i,
    input wire[`RegBus]         reg2_i,

    //for exterior data ram
    input wire[`RegBus]          mem_data_i,

    output reg[`RegBus]         mem_addr_o,
    output reg[`ByteSelBus]     mem_sel_o,//decide which Byte to choose
    output reg[`RegBus]         mem_data_o,
    output reg                  mem_ce_o//decide whether to work(enable)                  
    );

    
    always@(*) begin
        if(rst == `RstEnable) begin
            wd_o        = `NOPRegAddr;
            wreg_o      = `WriteDisable;
            wdata_o     = `ZeroWord;
            mem_addr_o  = `ZeroWord;
            mem_sel_o   = 4'b0000;
            mem_data_o  = `ZeroWord;
            mem_ce_o    = `ChipDisable;
        end else begin 
            wd_o    = wd_i;
            wreg_o  = wreg_i;
            wdata_o = wdata_i;
            mem_addr_o  = `ZeroWord;
            mem_sel_o   = 4'b0000;
            mem_data_o  = `ZeroWord;
            mem_ce_o    = `ChipDisable;
            case (aluop_i)
                `EXE_LB_OP:begin
                    mem_addr_o  =mem_addr_i;
                    mem_ce_o    =`ChipEnable;
                    wdata_o     ={{25{mem_data_i[7]}},mem_data_i[6:0]};
                end
                `EXE_LH_OP:begin
                    mem_addr_o  =mem_addr_i;
                    mem_ce_o    =`ChipEnable;
                    wdata_o     ={{17{mem_data_i[7]}},mem_data_i[14:0]};
                end
                `EXE_LW_OP:begin
                    mem_addr_o  =mem_addr_i;
                    mem_ce_o    =`ChipEnable;
                    wdata_o     =mem_data_i;  
                end 
                `EXE_LBU_OP:begin
                    mem_addr_o  =mem_addr_i;
                    mem_ce_o    =`ChipEnable;
                    wdata_o     ={{25{1'b0}},mem_data_i[6:0]};  
                end
                `EXE_LHU_OP:begin
                    mem_addr_o  =mem_addr_i;
                    mem_ce_o    =`ChipEnable;
                    wdata_o     ={{17{1'b0}},mem_data_i[14:0]};  
                end
                `EXE_SB_OP:
                begin
                    mem_addr_o  =mem_addr_i;
                    mem_ce_o    =`ChipEnable;
                    mem_data_o  =reg2_i;
                    mem_sel_o   =4'b0001;
                end
                `EXE_SH_OP:
                begin
                    mem_addr_o  =mem_addr_i;
                    mem_ce_o    =`ChipEnable;
                    mem_data_o  =reg2_i;
                    mem_sel_o   =4'b0011;
                end
                `EXE_SW_OP:
                begin
                    mem_addr_o  =mem_addr_i;
                    mem_ce_o    =`ChipEnable;
                    mem_data_o  =reg2_i;
                    mem_sel_o   =4'b1111;
                end  
                default:begin
                end 
            endcase
        end
    end
endmodule
