`include "define.v"
module stop(
    input wire clk,
    input wire rst,
    input wire branch_flag_i,
    input wire stallreq,
    output reg stop_flag_o
    );
    
    always @(posedge clk)begin
        if(rst == `RstEnable) begin
            stop_flag_o <= 1'b0;
        end if(stop_flag_o == `Stop) begin
            stop_flag_o <= 1'b0;
        end
        else begin
            stop_flag_o <= branch_flag_i & (~stallreq);
        end
    end
endmodule
