`include "define.v"

module BUS(
    //data from and to CPU
    input                     cpu_rom_ce_i,
    input      [`InstAddrBus] cpu_rom_addr_i,
    output     [`InstBus]     cpu_rom_data_o,
    input                     cpu_ram_ce_i,
    input      [`ByteSelBus]  cpu_ram_we_i,
    input      [`DataAddrBus] cpu_ram_addr_i,
    input      [`DataBus]     cpu_ram_din_i,
    output reg [`DataBus]     cpu_ram_dout_o,
    //data from and to ROM
    output                    rom_ce_o,
    output     [`RomAddrBus]  rom_addr_o,
    input      [`InstBus]     rom_data_i,
    //data from and to RAM
    output reg                ram_ce_o,
    output     [`ByteSelBus]  ram_we_o,
    output     [`RamAddrBus]  ram_addr_o,
    output     [`DataBus]     ram_din_o,
    input      [`DataBus]     ram_dout_i,
    //data from input
    input      [14:0]         sw_i,
    output reg [4:0]          btn_ena_o,
    input      [4:0]          btn_data_i,
    //data to output
    output reg                led_ena_o,
    output     [`LedBus]      led_din_o,
    output reg                tube_ena_o,
    output     [`TubeDinBus]  tube_din_o,
    output reg                menu_ena_o,
    output     [1:0]          menu_ctrl_o
    );

    //instruction bus
    assign rom_ce_o       = cpu_rom_ce_i;
    assign rom_addr_o     = cpu_rom_addr_i[`RomAddrBusSel];
    assign cpu_rom_data_o = rom_data_i;

    //data bus
    always @(*)
    begin
        if(cpu_ram_ce_i == `ChipEnable) begin
            ram_ce_o       = `ChipDisable;
            cpu_ram_dout_o = `ZeroWord;
            btn_ena_o      = {5{`ChipDisable}};
            led_ena_o      = `ChipDisable;
            tube_ena_o     = `ChipDisable;
            menu_ena_o     = `ChipDisable;
            case(cpu_ram_addr_i[`DvcSelBusSel])
                `DS_RAM:  begin
                    ram_ce_o  = `ChipEnable;
                    if(cpu_ram_we_i == {4{`WriteDisable}})
                        cpu_ram_dout_o = ram_dout_i;
                    else
                        cpu_ram_dout_o = `ZeroWord;
                end
                `DS_BTN:  begin
                    btn_ena_o = cpu_ram_addr_i[4:0];
                    cpu_ram_dout_o = {27'b0, btn_data_i};
                end
                `DS_SW:   begin
                    cpu_ram_dout_o = {17'b0, sw_i};
                end
                `DS_LED:  begin
                    led_ena_o = `ChipEnable;
                end
                `DS_TUBE: begin
                    tube_ena_o = `ChipEnable;
                end
                `DS_VGA: begin
                    menu_ena_o = `ChipEnable;
                end
                default:  begin
                end
            endcase
        end else begin
            ram_ce_o       = `ChipDisable;
            cpu_ram_dout_o = `ZeroWord;
            btn_ena_o      = {5{`ChipDisable}};
            led_ena_o      = `ChipDisable;
            tube_ena_o     = `ChipDisable;
            menu_ena_o     = `ChipDisable;
        end
    end
    assign ram_we_o    = cpu_ram_we_i;
    assign ram_addr_o  = cpu_ram_addr_i[`RamAddrBus];
    assign ram_din_o   = cpu_ram_din_i;
    assign led_din_o   = cpu_ram_din_i[`LedBus];
    assign tube_din_o  = cpu_ram_din_i[`TubeDinBus];
    assign menu_ctrl_o = cpu_ram_din_i[1:0];

endmodule
