`include "define.v"

module id(
    input wire rst,
    
    input wire[`InstAddrBus]    pc_i,
    input wire[`InstBus]        inst_i,
    
    //data from regfile
    input wire[`RegBus]         reg1_data_i,
    input wire[`RegBus]         reg2_data_i,
    
    //data to regfile
    output reg                  reg1_read_o,
    output reg                  reg2_read_o,
    output reg[`RegAddrBus]     reg1_addr_o,
    output reg[`RegAddrBus]     reg2_addr_o,
    
    //data to the ex module
    output reg[`AluOpBus]       aluop_o,
    output reg[`AluSelBus]      alusel_o,
    output reg[`RegBus]         reg1_o,
    output reg[`RegBus]         reg2_o,
    output reg[`RegAddrBus]     wd_o,
    output reg                  wreg_o,
    output wire[`InstBus]       inst_o,            
    
    //result from ex module
    input wire                  ex_wreg_i,
    input wire[`RegBus]         ex_wdata_i,
    input wire[`RegAddrBus]     ex_wd_i,
    input wire [`AluSelBus]     ex_alusel_i,
    
    //messege from stop
    input wire stop_flag_i,
    
    //result from mem module
    input wire                  mem_wreg_i,
    input wire[`RegBus]         mem_wdata_i,
    input wire[`RegAddrBus]     mem_wd_i,
    
    output reg                  branch_flag_o,
    output reg[`RegBus]         branch_target_address_o,
    output reg[`RegBus]         link_addr_o,
    
    //signal to stop
    output wire                 stallreq
    );
    wire [4:0]  shamt=inst_i[24:20];
    wire [4:0]  rs2=inst_i[24:20];
    wire [4:0]  rs1=inst_i[19:15];
    wire [2:0]  funct=inst_i[14:12];
    wire [4:0]  rd=inst_i[11:7];
    wire [6:0]  opcode=inst_i[6:0];
    
    //saving the immediate
    reg[`RegBus] imm;
    
    //show that if the instuction is valid
    reg instvalid;
    
    //saving the address of next instruction
    wire[`RegBus] pc_next;
    
    wire[`RegBus] imm_sllB_signdext; //deal with the imm data when the B-type instruction
    wire[`RegBus] imm_sllJ_signdext; //deal with the imm data when the jal instruction
    wire[`RegBus] imm_sllJI_signdext; //deal with the imm data when the jalr instruction
    wire[`RegBus] imm_sllU_signdext; //deal with the imm data when the lui instruction
    wire[`RegBus] imm_sllUPC_signdext; //deal with the imm data when the auipc instruction
    
    //deal with loadrelate
    reg stallreq_for_reg1_loadrelate;
    reg stallreq_for_reg2_loadrelate;
    wire pre_inst_is_load;
     
    assign pc_next = pc_i + 32'h4;
    assign imm_sllB_signdext={{20{inst_i[31]}},inst_i[7],inst_i[30:25],inst_i[11:8],1'b0};
    assign imm_sllJ_signdext={{12{inst_i[31]}},inst_i[19:12],inst_i[20],inst_i[30:21],1'b0};
    assign imm_sllJI_signdext={{21{inst_i[31]}},inst_i[30:20]};
    assign imm_sllU_signdext={inst_i[31:12],{12{1'b0}}};
    assign imm_sllUPC_signdext={inst_i[31:12],{12{1'b0}}}+pc_i;

    assign inst_o=inst_i;//deal with issues of load or store instructions

    assign pre_inst_is_load = (ex_alusel_i == `EXE_RES_LOAD)?`True_v:`False_v;
//----------decode section----------
    always@(*) begin
        if(rst == `RstEnable) begin
            aluop_o     = `EXE_NOP_OP;
            alusel_o    = `EXE_RES_NOP;
            wd_o        = `NOPRegAddr;
            wreg_o      = `WriteDisable;
            instvalid   = `InstValid;
            reg1_read_o = `ReadDisable;
            reg2_read_o = `ReadDisable;
            reg1_addr_o = `NOPRegAddr;
            reg2_addr_o = `NOPRegAddr;
            imm         = `ZeroWord;
            link_addr_o =`ZeroWord;
        end else begin
            aluop_o     = `EXE_NOP_OP;
            alusel_o    = `EXE_RES_NOP;
            wd_o        = rd;
            wreg_o      = `WriteDisable;
            instvalid   = `InstValid;
            reg1_read_o = `ReadDisable;
            reg2_read_o = `ReadDisable;
            reg1_addr_o = rs1;
            reg2_addr_o = rs2;
            imm         = `ZeroWord;
            link_addr_o = `ZeroWord;
            case(opcode)
                `EXE_R_INST:begin
                    case(funct)
                        `EXE_FUNCT_AND:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_AND_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_OR:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_OR_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_XOR:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_XOR_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_SLL:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_SHIFT;
                            aluop_o     = `EXE_SLL_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_SRL:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_SHIFT;
                            aluop_o     = inst_i[30]?`EXE_SRA_OP:`EXE_SRL_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_SLT:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_ARITHMETIC;
                            aluop_o     = `EXE_SLT_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_SLTU:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_ARITHMETIC;
                            aluop_o     = `EXE_SLTU_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_ADD:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_ARITHMETIC;
                            aluop_o     = inst_i[30]?`EXE_SUB_OP:`EXE_ADD_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        default:begin
                        end
                        
                    endcase
                end
                `EXE_I_INST:begin
                    case(funct)
                        `EXE_FUNCT_AND:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_AND_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            imm         = {{20{inst_i[31]}},inst_i[31:20]};
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_OR:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_OR_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            imm         = {{20{inst_i[31]}},inst_i[31:20]};
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_XOR:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_XOR_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            imm         = {{20{inst_i[31]}},inst_i[31:20]};
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_SLL:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_SHIFT;
                            aluop_o     = `EXE_SLL_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            imm         = {27'b0,shamt};
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_SRL:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_SHIFT;
                            aluop_o     = inst_i[30]?`EXE_SRA_OP:`EXE_SRL_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            imm         = {27'b0,shamt};
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_SLT:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_ARITHMETIC;
                            aluop_o     = `EXE_SLT_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            imm         = {{20{inst_i[31]}},inst_i[31:20]};
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_SLTU:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_ARITHMETIC;
                            aluop_o     = `EXE_SLTU_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            imm         = {{20{1'b0}},inst_i[31:20]};
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_ADD:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_ARITHMETIC;
                            aluop_o     = `EXE_ADD_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            imm         = {{20{inst_i[31]}},inst_i[31:20]};
                            instvalid   = `InstValid;
                        end
                        default:begin
                        end
                    endcase //case function
                end
                `EXE_B_INST:begin
                    case(funct)
                        `EXE_FUNCT_BEQ:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_SAVE_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_BNE:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_SAVE_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_BLT:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_SAVE_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_BGE:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_SAVE_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_BLTU:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_SAVE_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                        `EXE_FUNCT_BGEU:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_LOGIC;
                            aluop_o     = `EXE_SAVE_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end
                    endcase
                end
                `EXE_J_INST:begin
                    wreg_o      = `WriteEnable;
                    alusel_o    = `EXE_RES_JUMP;
                    reg1_read_o = `ReadDisable;
                    reg2_read_o = `ReadDisable;
                    link_addr_o = pc_next;
                end
                `EXE_JI_INST:begin
                    wreg_o      = `WriteEnable;
                    alusel_o    = `EXE_RES_JUMP;
                    reg1_read_o = `ReadEnable;
                    reg2_read_o = `ReadDisable;
                    link_addr_o = pc_next;
                end
                `EXE_U_INST:begin
                    wreg_o      = `WriteEnable;
                    alusel_o    = `EXE_RES_LOGIC;
                    aluop_o     = `EXE_SAVE_OP;
                    reg1_read_o = `ReadDisable;
                    reg2_read_o = `ReadDisable;
                    imm         = imm_sllU_signdext;
                    instvalid   = `InstValid;
                end
                `EXE_UPC_INST:begin
                    wreg_o      = `WriteEnable;
                    alusel_o    = `EXE_RES_LOGIC;
                    aluop_o     = `EXE_SAVE_OP;
                    reg1_read_o = `ReadDisable;
                    reg2_read_o = `ReadDisable;
                    imm         = imm_sllUPC_signdext;
                    instvalid   = `InstValid;
                end
                default:begin
                end
                `EXE_IL_INST:begin
                    case (funct)
                        `EXE_FUNCT_LB:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOAD;
                            aluop_o     = `EXE_LB_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            instvalid   = `InstValid;
                        end 
                        `EXE_FUNCT_LH:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOAD;
                            aluop_o     = `EXE_LH_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            instvalid   = `InstValid;
                        end 
                        `EXE_FUNCT_LW:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOAD;
                            aluop_o     = `EXE_LW_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            instvalid   = `InstValid;
                        end 
                        `EXE_FUNCT_LBU:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOAD;
                            aluop_o     = `EXE_LBU_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            instvalid   = `InstValid;
                        end 
                        `EXE_FUNCT_LHU:begin
                            wreg_o      = `WriteEnable;
                            alusel_o    = `EXE_RES_LOAD;
                            aluop_o     = `EXE_LHU_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadDisable;
                            instvalid   = `InstValid;
                        end 
                        default:begin
                        end 
                    endcase
                end
                `EXE_S_INST:begin
                    case(funct)
                        `EXE_FUNCT_SB:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_STORE;
                            aluop_o     = `EXE_SB_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end 
                        `EXE_FUNCT_SH:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_STORE;
                            aluop_o     = `EXE_SH_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end 
                        `EXE_FUNCT_SW:begin
                            wreg_o      = `WriteDisable;
                            alusel_o    = `EXE_RES_STORE;
                            aluop_o     = `EXE_SW_OP;
                            reg1_read_o = `ReadEnable;
                            reg2_read_o = `ReadEnable;
                            instvalid   = `InstValid;
                        end 
                    endcase
                end    
            endcase //case opcode
        end         //if
    end             //always
    
    always@(*) begin
        if(rst == `RstEnable) begin
            branch_flag_o = `NotBranch;
            branch_target_address_o =`ZeroWord;
        end if(stop_flag_i == `Stop) begin
            branch_flag_o = `NotBranch;
            branch_target_address_o =`ZeroWord;
        end else begin
            branch_flag_o = `NotBranch;
            branch_target_address_o =`ZeroWord;
            case(opcode)
                `EXE_B_INST:begin
                    case(funct)
                        `EXE_FUNCT_BEQ:begin;
                            if(reg1_o == reg2_o) begin
                                branch_flag_o = `Branch;
                                branch_target_address_o = pc_i + imm_sllB_signdext;
                            end
                        end
                        `EXE_FUNCT_BNE:begin
                            if(reg1_o != reg2_o) begin
                                branch_flag_o = `Branch;
                                branch_target_address_o = pc_i + imm_sllB_signdext;
                            end
                        end
                        `EXE_FUNCT_BLT:begin
                            if((reg1_o[31]&& !reg2_o[31] )||((reg1_o[31] == reg2_o[31]) && (reg1_o[30:0] < reg2_o[30:0]))) begin
                                branch_flag_o = `Branch;
                                branch_target_address_o = pc_i + imm_sllB_signdext;
                            end
                        end
                        `EXE_FUNCT_BGE:begin
                            if((!reg1_o[31]&& reg2_o[31] )||((reg1_o[31] == reg2_o[31]) && (reg1_o[30:0] >= reg2_o[30:0]))) begin
                                branch_flag_o = `Branch;
                                branch_target_address_o = pc_i + imm_sllB_signdext;
                            end
                        end
                        `EXE_FUNCT_BLTU:begin
                            if(reg1_o < reg2_o) begin
                                branch_flag_o = `Branch;
                                branch_target_address_o = pc_i + imm_sllB_signdext;
                            end
                        end
                        `EXE_FUNCT_BGEU:begin
                            if(reg1_o >= reg2_o) begin
                                branch_flag_o = `Branch;
                                branch_target_address_o = pc_i + imm_sllB_signdext;
                            end
                        end
                        default:begin
                                branch_target_address_o = `ZeroWord;
                                branch_flag_o = `NotBranch;
                        end 
                    endcase
                end
                `EXE_J_INST:begin
                    branch_target_address_o = imm_sllJ_signdext+pc_i;
                    branch_flag_o = `Branch;
                end
                `EXE_JI_INST:begin
                    branch_target_address_o = imm_sllJI_signdext + reg1_o;
                    branch_flag_o = `Branch;
                end
                default:begin
                    branch_target_address_o = `ZeroWord;
                    branch_flag_o = `NotBranch;
                end 
            endcase
        end
    end
    
    //decide the first number
    always@(*)begin
        reg1_o = `ZeroWord;
        stallreq_for_reg1_loadrelate = `False_v;
        if(rst == `RstEnable) begin
            reg1_o = `ZeroWord;
        end else if((pre_inst_is_load == `True_v)&&(reg1_read_o == `True_v)&&(ex_wd_i == reg1_addr_o)) begin
            stallreq_for_reg1_loadrelate = `True_v;
        end else if((reg1_read_o == `True_v)&&(ex_wreg_i ==`True_v)&&(ex_wd_i == reg1_addr_o)) begin
            reg1_o = ex_wdata_i;
        end else if((reg1_read_o == `True_v)&&(mem_wreg_i ==`True_v)&&(mem_wd_i == reg1_addr_o)) begin
            reg1_o = mem_wdata_i;
        end else if(reg1_read_o == `True_v) begin
            reg1_o = reg1_data_i;
        end else if(reg1_read_o == `False_v) begin
            reg1_o = imm;
        end else begin
            reg1_o = `ZeroWord;
        end
    end
    
    //decide the second number
    always@(*)begin
        reg2_o = `ZeroWord;
        stallreq_for_reg2_loadrelate = `False_v;
        if(rst == `RstEnable) begin
            reg2_o = `ZeroWord;
        end else if((pre_inst_is_load == `True_v)&&(reg2_read_o == `True_v)&&(ex_wd_i == reg2_addr_o)) begin
            stallreq_for_reg2_loadrelate = `True_v;
        end  else if((reg2_read_o == `True_v)&&(ex_wreg_i ==`True_v)&&(ex_wd_i == reg2_addr_o)) begin
            reg2_o = ex_wdata_i;
        end else if((reg2_read_o == `True_v)&&(mem_wreg_i ==`True_v)&&(mem_wd_i == reg2_addr_o)) begin
            reg2_o = mem_wdata_i;
        end else if(reg2_read_o == `True_v) begin
            reg2_o = reg2_data_i;
        end else if(reg2_read_o == `False_v) begin
            reg2_o = imm;
        end else begin
            reg2_o = `ZeroWord;
        end
    end
    assign stallreq = stallreq_for_reg2_loadrelate | stallreq_for_reg1_loadrelate;
endmodule
