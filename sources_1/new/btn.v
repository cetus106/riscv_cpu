module BTN(
    input        clk,
    input        rst,
    input  [4:0] ena,   //Signal of signal received by CPU
    input  [4:0] din,
    output [4:0] dout
    );
    BTN_PROC BTN_PROC0(
        .clk(clk),      .rst(rst),      .ena(ena[0]),
        .din(din[0]),   .dout(dout[0])
    );
    BTN_PROC BTN_PROC1(
        .clk(clk),      .rst(rst),      .ena(ena[1]),
        .din(din[1]),   .dout(dout[1])
    );
    BTN_PROC BTN_PROC2(
        .clk(clk),      .rst(rst),      .ena(ena[2]),
        .din(din[2]),   .dout(dout[2])
    );
    BTN_PROC BTN_PROC3(
        .clk(clk),      .rst(rst),      .ena(ena[3]),
        .din(din[3]),   .dout(dout[3])
    );
    BTN_PROC BTN_PROC4(
        .clk(clk),      .rst(rst),      .ena(ena[4]),
        .din(din[4]),   .dout(dout[4])
    );
endmodule
