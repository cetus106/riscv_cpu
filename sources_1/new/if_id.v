`include "define.v"

module if_id(
    input wire clk,
    input wire rst,
    
    //loadrelate message from id
    input wire                  stallreq,
    
    //signal from pc module
    input wire[`InstAddrBus]        if_pc,
    input wire[`InstBus]            if_inst,
    
    //signal to id module
    output reg[`InstAddrBus]       id_pc,
    output reg[`InstBus]           id_inst
    );
    always@(posedge clk) begin
        if(rst == `RstEnable) begin
            id_pc <= `ZeroWord;
            id_inst <= `ZeroWord;       //instruction = 0 if rst
        end else if(stallreq == `Stop) begin
            id_pc <= id_pc;
            id_inst <= id_inst;
        end else begin
            id_pc <= if_pc;             //transmit instruction if not rst
            id_inst <= if_inst;         
        end
    end
endmodule
