`include "define.v"

module TUBE(
    input                clk_u,  //clock for update
    input                clk_d,  //clock for display
    input                clk_f,  //clock for flash
    input                rst,
    input                ena,    //update enable
    input  [`TubeDinBus] din,
    output [11:0]        dout
    );

    reg [3:0] dgt_active;
    reg [3:0] dgt_flash;
    reg [3:0] dot;
    reg [3:0] num_3;
    reg [3:0] num_2;
    reg [3:0] num_1;
    reg [3:0] num_0;

    reg [3:0] dgt_o;    //zero active
    reg [6:0] sgm_o;    //zero active
    reg       dot_o;    //zero active
    assign dout = {dgt_o, sgm_o, dot_o};

    reg [1:0] dgt_sel;
    wire[3:0] dgt_on;   //zero active
    reg [3:0] num;

    always @(posedge clk_u)
    begin
        if(rst) begin
            dgt_active <= 4'b0;
            dgt_flash  <= 4'b0;
            dot        <= 4'b0;
            num_3      <= 4'd0;
            num_2      <= 4'd0;
            num_1      <= 4'd0;
            num_0      <= 4'd0;
        end
        else if(ena) begin
            dgt_active <= din[27:24];
            dgt_flash  <= din[23:20];
            dot        <= din[19:16];
            num_3      <= din[15:12];
            num_2      <= din[11: 8];
            num_1      <= din[ 7: 4];
            num_0      <= din[ 3: 0];
        end
        else begin
            dgt_active <= dgt_active;
            dgt_flash  <= dgt_flash;
            dot        <= dot;
            num_3      <= num_3;
            num_2      <= num_2;
            num_1      <= num_1;
            num_0      <= num_0;
        end
    end

    assign dgt_on = ~( dgt_active & (~dgt_flash | (dgt_flash&{4{clk_f}})) );

    always @(posedge clk_d or posedge rst)
    begin
        if(rst) dgt_sel<=2'b0;
        else dgt_sel <= dgt_sel + 2'd1;
    end

    always @(posedge clk_d or posedge rst)
    begin
        if(rst) {dgt_o,num,dot_o}<=9'b111100001;
        else begin
        case(dgt_sel)
        2'd0: begin
                    dgt_o <= { 3'b111, dgt_on[0] }; 
                    num <= num_0;
                    dot_o <= !dot[0];
              end
        2'd1: begin
                    dgt_o <= { 2'b11, dgt_on[1],1'b1 }; 
                    num <= num_1;
                    dot_o <= !dot[1];
              end
        2'd2: begin
                    dgt_o <= { 1'b1, dgt_on[2], 2'b11 }; 
                    num <= num_2;
                    dot_o <= !dot[2];
              end
        2'd3: begin
                    dgt_o <= { dgt_on[3], 3'b111 }; 
                    num <= num_3;
                    dot_o <= !dot[3];
              end
        endcase
        end
    end

    always @(*)
    begin
        case(num)
        4'd0: sgm_o = 7'b0000001;
        4'd1: sgm_o = 7'b1001111;
        4'd2: sgm_o = 7'b0010010;
        4'd3: sgm_o = 7'b0000110;
        4'd4: sgm_o = 7'b1001100;
        4'd5: sgm_o = 7'b0100100;
        4'd6: sgm_o = 7'b0100000;
        4'd7: sgm_o = 7'b0001111;
        4'd8: sgm_o = 7'b0000000;
        4'd9: sgm_o = 7'b0000100;
        default: sgm_o = 7'b0000001;
        endcase
    end
    
endmodule
