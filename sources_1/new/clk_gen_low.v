module CLK_GEN_LOW #(
    parameter DIVISOR = 1_0000_0000)( //frequency_out = frequency_in / DIVISOR
    input  clk_i,
    input  rst,
    output reg clk_o
    );
    reg [26:0] cnt;
    always @(posedge clk_i or posedge rst)
    begin
        if(rst) begin
            cnt<=27'd0;
            clk_o<=1'b0;
        end
        else if(cnt == (DIVISOR/2 - 1))
        begin
            clk_o <= !clk_o;
            cnt <= 27'd0;
        end
        else
            cnt <= cnt + 27'd1;
    end
endmodule
