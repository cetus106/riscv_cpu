// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Mon Jan  8 10:58:58 2024
// Host        : JYKsMateBook16s running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/Data_of_edu_software/Vivado_Projects/honor_pojects/riscv_cpu/riscv_cpu.srcs/sources_1/ip/CLK_GEN/CLK_GEN_stub.v
// Design      : CLK_GEN
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
module CLK_GEN(clk_o1, clk_o2, clk_i)
/* synthesis syn_black_box black_box_pad_pin="clk_o1,clk_o2,clk_i" */;
  output clk_o1;
  output clk_o2;
  input clk_i;
endmodule
