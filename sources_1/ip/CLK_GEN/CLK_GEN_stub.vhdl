-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Mon Jan  8 10:58:58 2024
-- Host        : JYKsMateBook16s running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               D:/Data_of_edu_software/Vivado_Projects/honor_pojects/riscv_cpu/riscv_cpu.srcs/sources_1/ip/CLK_GEN/CLK_GEN_stub.vhdl
-- Design      : CLK_GEN
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a35tcpg236-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity CLK_GEN is
  Port ( 
    clk_o1 : out STD_LOGIC;
    clk_o2 : out STD_LOGIC;
    clk_i : in STD_LOGIC
  );

end CLK_GEN;

architecture stub of CLK_GEN is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_o1,clk_o2,clk_i";
begin
end;
