// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sun Jan  7 15:16:36 2024
// Host        : JYKsMateBook16s running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               D:/Data_of_edu_software/Vivado_Projects/honor_pojects/riscv_cpu/riscv_cpu.srcs/sources_1/ip/word_picture/word_picture_stub.v
// Design      : word_picture
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a35tcpg236-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_3,Vivado 2019.1" *)
module word_picture(clka, addra, douta)
/* synthesis syn_black_box black_box_pad_pin="clka,addra[18:0],douta[0:0]" */;
  input clka;
  input [18:0]addra;
  output [0:0]douta;
endmodule
