`include "../../sources_1/new/define.v"
`timescale  1ns / 1ps
module tb_CPU;

// CPU Inputs
reg   clk                                  = 0 ;
reg   rst                                  = 0 ;
reg   [`InstBus]     rom_data_i            = 0 ;
reg   [`DataBus]     ram_data_i            = 0 ;

// CPU Outputs
wire  rom_ce_o                             ;
wire  [`InstAddrBus] rom_addr_o            ;
wire  ram_ce_o                             ;
wire  [`ByteSelBus]  ram_we_o              ;
wire  [`DataAddrBus] ram_addr_o            ;
wire  [`DataBus]     ram_data_o            ;


always #5  clk=~clk;

CPU  u_CPU (
    .clk                        ( clk                         ),
    .rst                        ( rst                         ),
    .rom_data_i                 ( rom_data_i                  ),
    .ram_data_i                 ( ram_data_i                  ),

    .rom_ce_o                   ( rom_ce_o                    ),
    .rom_addr_o                 ( rom_addr_o                  ),
    .ram_ce_o                   ( ram_ce_o                    ),
    .ram_we_o                   ( ram_we_o                    ),
    .ram_addr_o                 ( ram_addr_o                  ),
    .ram_data_o                 ( ram_data_o                  )
);

initial
begin
    #20 rst  =  1;
    #20 rst  =  0;
    #6              //posedge trigger

//----------testbench for L instruction----------
/*
    //-----------testbench for load byte instruction----------
    ram_data_i = 32'b00000001_00000010_00000100_00001000;
    rom_data_i = 32'b1111111_11111_00000_000_00001_0010011;//addi x1,x0,-1
    #10
    rom_data_i = 32'b0000000_00010_00001_000_00011_0000011;//lb x3,1,x1
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
    //LB instruction succeed
*/
/*
    //-----------testbench for LH instruction----------
    ram_data_i = 32'b00000001_00000010_00000100_00001000;
    rom_data_i = 32'b00000000000000000000000010010011;//addi x1,x0,0
    #10
    rom_data_i = 32'b00000000010000001001000110000011;//lh x3,4,x1
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
    //LH instruction succeed
*/
    //-----------testbench for LW instruction---------- jyk have detected before
    //LW instruction succeed

/*
//----------testbench for S instruction
    ////-----------testbench for SB instruction----------
    rom_data_i = 32'b11111111111100000000000010010011;//addi x1,x0,-1
    #10
    rom_data_i = 32'b 00000000000100000000001000100011;//sb x1,4,x0
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
    //S instruction  succeed
*/


//----------testbench for I instruction
    /*----------addi and ori succeed-----------
        rom_data_i = 32'b0000000_00001_00010_000_00011_0010011; //addi  x3, x2, 1
        #10
        rom_data_i = 32'b0000000_00001_00011_110_00100_0110011; //or    x4, x3, x1
        #10
        rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
        #70
    */

    //xori should be successful

    /*----------slti succeed ----------
    rom_data_i = 32'b11111111111100000000000010010011;//addi x1,x0,-1
    #10
    rom_data_i = 32'b00000000001000001010000100010011;//slti x2,x1,2
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
    */

    /*----------sltiu succeed----------
    rom_data_i = 32'b11111111111100000000000010010011;//addi x1,x0,-1
    #10
    rom_data_i = 32'b00000000001000001011000100010011;//sltiu x2,x1,2
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
    */

    /*----------slli succeed----------
    rom_data_i = 32'b11111111111100000000000010010011;//addi x1,x0,-1
    #10
    rom_data_i = 32'b00000000000100001001000100010011;//slli x2,x1,1
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
    */
    //srli should be successful

    /*----------srai succeed----------
    rom_data_i = 32'b11111111111100000000000010010011;//addi x1,x0,-1
    #10
    rom_data_i = 32'b01000000000100001101000100010011;//srai x2,x1,1
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
    */


//testbench for R instruction
/*
    //add instruction and sub instruction succeed
    rom_data_i = 32'b11111111111100000000000010010011;//addi x1,x0,-1
    #10
    rom_data_i = 32'b00000000001100001000000100010011;//addi x2,x1,3
    #10
    rom_data_i = 32'b0000000_00010_00001_000_00011_0110011;//add  x3,x1,x2
    #10
    rom_data_i = 32'b0100000_00001_00010_000_00100_0110011;//sub  x4,x2,x1
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50

//the rest of R instruction have not been tested. 
*/

/*
//testbench for LUI instruction ---succeed
    rom_data_i = 32'b00000000000000000001000010110111;//lui x1,1
    #10
    rom_data_i = 32'b00000000000011111111000100110111;//lui x2,255
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50

*/

/*
//-----------testbench for AUIPC instruction
    // it's correct 
    rom_data_i = 32'b00000000000000000000000010010111;//auipc x1,0
    #10
    rom_data_i = 32'b00000000000000000000000100010111;//auipc x2,0
    #10
    rom_data_i = 32'b00000000000000000000000110010111;//auipc x3,0
    #10
    rom_data_i = 32'b00000000000000000100001000010111;//auipc x4,4
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
*/


//----------testbench for J instruction
    /*
    // JAL instruction succeed
    rom_data_i = 32'b00000000000000000000000100010111;//auipc x2,0
    #10
    rom_data_i = 32'b00000000000000000000000110010111;//auipc x3,0
    #10
    rom_data_i = 32'b00000000000000000100001000010111;//auipc x4,4
    #10
    rom_data_i = 32'b11111111010111111111000011101111;//jal x1,-12
    #10
    rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    #50
    */

    // JALR instruction
    //rom_data_i = 32'b00000000100000000000000010010011;//addi x1,x0,8
    //#10
    /*
    rom_data_i = 32'b00000000000000000000000100010111;//auipc x2,0
    #10
    rom_data_i = 32'b00000000000000000000000110010111;//auipc x3,0
    #10
    rom_data_i = 32'b00000000000000000100001000010111;//auipc x4,4
    #10
    rom_data_i = 32'b00000000010000001000001011100111;//jalr x5,x1,4
    #10
    */
    //rom_data_i = 32'b0000000_00000_00000_000_00000_0000000; //nop
    //#50
 
    $finish;

end



endmodule
