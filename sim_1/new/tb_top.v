`timescale  1ns / 1ps

module tb_top_module;

// top_module Inputs
reg   clk = 0 ;
reg   rst = 0 ;

// top_module Outputs
reg   [4:0]   btn = 5'b0;
reg   [14:0]  sw  = 15'b0;
wire  [15:0]  led;
wire  [11:0]  tube;
wire  [13:0]  vga;

always #5  clk=~clk;

top_module  u_top_module (
    .clk    ( clk  ),
    .rst    ( rst  ),
    .btn    ( btn  ),
    .sw     ( sw   ),
    .led    ( led  ),
    .tube   ( tube ),
    .vga    ( vga  )
);

initial
begin
           rst  = 1;
    #3000  rst  = 0;        //It requires about 2700ns for CLK_GEN to get prepared
    #7000  btn  = 5'b00100; //Down button
    #10000 btn  = 5'b00000;
    #5000  btn  = 5'b10000; //Central button
    #10000 btn  = 5'b00000;
    #5000  btn  = 5'b01000; //Up button
    #10000 btn  = 5'b00000;
    #30000
    $finish;
end

endmodule